var plivo = require('plivo');

var response = plivo.Response();

var first_speak_body ="Please leave a message after the beep. Press the star key when done.";
response.addSpeak(first_speak_body);

var params = {
    'action': "https://www.foo.com/get_recording/",
    'maxLength': "30",
    'finishOnKey': "*"
};
response.addRecord(params);

var second_speak_body = "Recording not received.";
response.addSpeak(second_speak_body);

console.log(response.toXML());
