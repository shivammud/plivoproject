var plivo = require('plivo');

var response = plivo.Response();

var speak_body = "Go Green, Go Plivo.";
response.addSpeak(speak_body);

console.log(response.toXML());

/*
Sample Output
<Response>
    <Speak>Go Green, Go Plivo.</Speak>
</Response>
*/